<?php
    class Circle{
        private $diameter;
        public $demo;
        /**
         * Circle constructor.
         * @param $diameter
         */
        public function __construct($diameter)
        {
            $this->diameter = $diameter;
        }

        public function __toString( )
        {
            // TODO: Implement __toString() method.
            return "Area: " . $this->calcArea() . "\nPerimeter:" . $this->calcPerimeter() . "\n";
        }

        //Calculate Area
        public function calcArea()
        {
            return round( $this->diameter *$this->diameter,2);
        }

        //Calculate Perimeter
        public function calcPerimeter()
        {
            return round($this->diameter * 3.14,2) ;
        }

        /**
         * @return mixed
         */
        public function getDiameter()
        {
            return $this->diameter ;
        }

        /**
         * @param mixed $diameter
         */
        public function setDiameter($diameter)
        {
            $this->diameter = $diameter;
        }

    }

    function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    if ($argv[1] > 0)
    {
        $shape = new Circle(trim(clean($argv[1])));
        echo $shape;
    }
    else
        echo "Please enter again \n";
    
?>

